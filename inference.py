# inference.py
# ------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and Pieter
# Abbeel in Spring 2013.
# For more info, see http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html

import itertools
import util
import random
import busters
import game
import random

from game import Directions, Actions

DEBUG_OUTPUT = False

def neighbours(position, gameState):
    adj = 0
    for action in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
        successorPosition = game.Actions.getSuccessor(position, action)
        if not gameState.hasWall(int(successorPosition[0]),int(successorPosition[1])):
            adj += 1

    return adj > 0

class InferenceModule:
    """
    An inference module tracks a belief distribution over pacman location.
    This is an abstract class, which you should not modify.
    """

    ############################################
    # Useful methods for all inference modules #
    ############################################

    def __init__(self, ghostAgent):
        "Sets the ghost agent for later access"
        self.ghostAgent = ghostAgent
        self.index = ghostAgent.index
        self.obs = [] # most recent observation position


    def getPacmanSuccesorDistribution(self, gameState, position):

        """
        Returns a distribution over successor positions of the pacman from the given gameState.
        No assumptions are made about the way Pacman is moving.
        """

        dist = util.Counter()

        for action in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
            successorPosition = game.Actions.getSuccessor(position, action)
            x, y = map(int, successorPosition)
            if not gameState.hasWall(x,y):
                dist[successorPosition] = 1

        dist.normalize()

        return dist

    def observeState(self, gameState):
        "Collects the relevant noisy distance observation and pass it along."

        noisyWalls = gameState.getNoisyWalls()

        self.observe(gameState, noisyWalls)

    def initialize(self, gameState):
        "Initializes beliefs to a uniform distribution over all positions."
        # The legal positions do not include the ghost prison cells in the bottom left.

        self.legalPositions = [p for p in gameState.getWalls().asList(False) if p[1] >= 3 and neighbours(p, gameState)]

        self.initializeUniformly(gameState)

    ######################################
    # Methods that need to be overridden #
    ######################################

    def initializeUniformly(self, gameState):
        "Sets the belief state to a uniform prior belief over all positions."
        pass

    def observe(self, gameState, observationWalls):
        "Updates beliefs based on the given noisy perception of the walls and gameState."
        pass

    def elapseTime(self, gameState):
        "Updates beliefs for a time step elapsing from a gameState."
        pass

    def getBeliefDistribution(self):
        """
        Returns the agent's current belief state, a distribution over
        ghost locations conditioned on all evidence so far.
        """
        pass

class ExactInference(InferenceModule):
    """
    The exact dynamic inference module should use forward-algorithm
    updates to compute the exact belief function at each time step.
    """

    def initializeUniformly(self, gameState):
        """
        Begin with a uniform distribution over possible pacman positions.
        You may find useful self.legalPositions which contains the possible
        positions in the map.
        """
        if DEBUG_OUTPUT:
            print "Initializing"
            print "Number of legal positions:", len(self.legalPositions)
            #print busters.getObservationDistributionNoisyWall
            print self.legalPositions
            #print gameState

        self.beliefs = util.Counter()

        for pos in self.legalPositions:
            self.beliefs[pos] = 1

        self.beliefs.normalize()

    def observe(self, gameState, observationWalls):
        """
        Updates self.beliefs based on the perception of the current
        *unknown* position of the pacman.

        The observationWalls is a noisy perception of whether is a wall or not
        in the four adjacent locations (North,Sout,East,West). This amount of
        noise is given by an Epsilon (See busters.EPSILON).

        self.legalPositions is a list of the possible pacman positions (you
        should only consider positions that are in self.legalPositions).

        Remember that you *don't* know the pacman position and you must not
        make any direct assumptions about it, just what you can infer from the
        perceptions.

        """
        if DEBUG_OUTPUT:
            print "Observing"
            #print dir(gameState)
            #print gameState.data.layout.walls
            #print observationWalls
            #print busters.getObservationDistributionNoisyWall(observationWalls)
            #print gameState.getWalls().asList(True)

        distObsdWalls = busters.getObservationDistributionNoisyWall(observationWalls)
        walls = gameState.getWalls().asList(True)

        for x,y in self.legalPositions:
            wallsInPos = (
                (Directions.NORTH, (x  , y+1) in walls),
                (Directions.SOUTH, (x  , y-1) in walls),
                (Directions.EAST,  (x+1, y  ) in walls),
                (Directions.WEST,  (x-1, y  ) in walls)
            )

            self.beliefs[(x,y)] *= distObsdWalls[ wallsInPos ]

        self.beliefs.normalize()

        if DEBUG_OUTPUT:
            print "Belief Distribution"
            print '\n'.join([ '%9s:%6s%s' % (k, '', self.beliefs[k]) for k in sorted(self.beliefs.keys()) ])
            print gameState

    def elapseTime(self, gameState):
        """
        Update self.beliefs in response to a time step passing from the current
        state.

        The model is updated to incorporate the knowledge acquired from the previous
        time. In order to obtain the distribution over new positions for the pacman,
        given its previous position (oldPos) use this line of code:

        newPostDist = self.getPacmanSuccesorDistribution(gameState,oldPos)

        Note that you may need to replace "oldPos" with the correct name of the
        variable that you have used to refer to the previous pacman position for
        which you are computing this distribution. You will need to compute
        multiple position distributions for a single update.

        newPosDist is a util.Counter object, where for each position p in
        self.legalPositions,

        newPostDist[p] = Pr( pacman is at position p at time t + 1 | pacman is at position oldPos at time t )
        """
        newBeliefs = util.Counter()

        for pos in self.legalPositions:
            prob = self.beliefs[pos]

            newPostDist = self.getPacmanSuccesorDistribution(gameState, pos)
            for newPos, newProb in newPostDist.items():
                newBeliefs[newPos] += newProb * prob

        #newBeliefs.normalize()

        self.beliefs = newBeliefs

    def getBeliefDistribution(self):
        return self.beliefs

class attrdict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self.__dict__ = self

class ParticleFilter(InferenceModule):
    """
    A particle filter for finding a single ghost.

    Useful helper functions will include random.choice, which chooses
    an element from a list uniformly at random, and util.sample, which
    samples a key from a Counter by treating its values as probabilities.
    """


    def __init__(self, ghostAgent, numParticles=1000):
        InferenceModule.__init__(self, ghostAgent);

        self.setNumParticles(numParticles)

    def setNumParticles(self, numParticles):
        self.numParticles = numParticles


    def initializeUniformly(self, gameState):
        """
          Initializes a list of particles. Use self.numParticles for the number of particles.
          Use self.legalPositions for the legal board positions where a particle could be located.
          Particles should be evenly (not randomly) distributed across positions in order to
          ensure a uniform prior.

          ** NOTE **
            the variable you store your particles in must be a list; a list is simply a collection
            of unweighted variables (positions in this case). Storing your particles as a Counter or
            dictionary (where there could be an associated weight with each position) is incorrect
            and will produce errors
        """
        if DEBUG_OUTPUT:
            print "Initializing"
            print "Number of particles:", self.numParticles
            print "Number of legal positions:", len(self.legalPositions)
            print gameState

        n = 0
        posN = 0
        totalPos = len(self.legalPositions)

        self.particles = []
        while n < self.numParticles:
            pos = self.legalPositions[posN]
            self.particles.append( pos )

            n += 1
            posN = (posN + 1) % totalPos

    def observe(self, gameState, observationWalls):
        """
        Update beliefs based on the given wall observation. Make sure to
        handle the special case where all particles have weight 0 after
        reweighting based on observation. If this happens, resample particles
        uniformly at random from the set of legal positions (self.legalPositions).

        Remember that when all particles receive 0 weight, they should be recreated from
        the prior distribution by calling initializeUniformly. The total
        weight for a belief distribution can be found by calling totalCount on a Counter object.
        """
        distObsdWalls = busters.getObservationDistributionNoisyWall(observationWalls)
        walls = gameState.getWalls().asList(True)

        if DEBUG_OUTPUT:
            print "Observing"
            #print gameState.data.layout.walls
            #print observationWalls
            #print '\n'.join([ '%72s:%6s%s' % (k, '', distObsdWalls[k]) for k in sorted(distObsdWalls.keys()) ])
            #print gameState

        weights = util.Counter()

        for particle in self.particles:
            x,y = particle
            wallsInPos = (
                (Directions.NORTH, (x  , y+1) in walls),
                (Directions.SOUTH, (x  , y-1) in walls),
                (Directions.EAST,  (x+1, y  ) in walls),
                (Directions.WEST,  (x-1, y  ) in walls)
            )

            weights[particle] += distObsdWalls[ wallsInPos ]

        if weights.totalCount() == 0:
            self.initializeUniformly(gameState)
        else:
            weights.normalize()
            #self.particles = [util.sample(weights) for i in range(self.numParticles)]
            self.particles = self.sample(weights, self.numParticles)

        if DEBUG_OUTPUT:
            print "Belief Distribution"
            belief = self.getBeliefDistribution()
            print '\n'.join([ '%9s:%6s%s' % (k, '', belief[k]) for k in sorted(belief.keys()) ])
            print gameState

    def sample(self, distribution, numSamples):
        """
        Receives a distribution in a Dictionary object and returns n (numSamples) samples from it.
        Assumes, distribution is a distribution (total sum is 1)
        """
        items = distribution.items()
        distribution = [i[1] for i in items]
        values = [i[0] for i in items]

        samples = []
        for j in range(numSamples):
            choice = random.random()
            i, total = 0, distribution[0]
            while choice > total:
                i += 1
                total += distribution[i]
            samples.append( values[i] )

        return samples

    def elapseTime(self, gameState):
        """
        Update beliefs for a time step elapsing.

        As in the elapseTime method of ExactInference, you should use:

        newPostDist = self.getPacmanSuccesorDistribution(gameState,oldPos)

        to obtain the distribution over new positions for the pacman, given its
        previous position (oldPos).

        util.sample(Counter object) is a helper method to generate a sample from
        a belief distribution.
        """
        for i in range(self.numParticles):
            particle = self.particles[i]
            newPostDist = self.getPacmanSuccesorDistribution(gameState, particle)
            self.particles[i] = util.sample(newPostDist)

    def getBeliefDistribution(self):
        """
        Return the agent's current belief state, a distribution over pacman
        locations conditioned on all evidence and time passage. This method
        essentially converts a list of particles into a belief distribution (a
        Counter object)
        """
        beliefs = util.Counter()

        for particle in self.particles:
            beliefs[particle] += 1

        beliefs.normalize()

        return beliefs
